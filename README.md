# flux

Flux configuration for the ATLAS Rucio Kubernetes deployment.

Instruction how to setup a new cluster can be found here: 
https://gitlab.cern.ch/atlas-adc-ddm/rucio-k8s-setup

# Install sops

Grab the latest release from: https://github.com/mozilla/sops/releases

The `linux.amd64` variant works on `aiadm` and can be dropped into your `$HOME/bin`. Don't forget to `chmod +x`.

# Adding secrets

Create the unencrypted kubernetes secret by starting with a new empty file, and put the following boilerplate:

    apiVersion: v1
    kind: Secret
    metadata:
        name: my-secret
        namespace: rucio
    type: Opaque
    stringData:
        my-secret: |
           this is the secret
           mult-line data
           that will be encrypted

From the example above it should be obvious where the secret content goes. Make sure that you indent it correctly!

Then encrypt it with the following command:

    sops --encrypt \
      --in-place\
      --age=age1gpmuhmxkkuvu09yaldrpa5qlag678g87230kad825d963ped75qsv588w9 \
      --encrypted-regex '^(data|stringData)$' \
      secret.yaml

The `age` is basically the identifier against which you will encrypt, so keep the proposed one. The `data/stringData` stanza is to make sure you don't encrypt the boilerplate as well.
